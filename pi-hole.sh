#!/bin/bash

CIP=$(curl -s https://api.ipify.org)


echo "AutoInstall Flared PiVPN by SANYUK.RU on $CIP"

cat > pi-hole-openvpn-cloudflare.yaml <<EOF

version: "3"

services:
  cloudflared:
    container_name: cloudflared
    image: visibilityspots/cloudflared:latest
    restart: always
    networks:
      sanyuk_ru:
        ipv4_address: 10.0.5.2
  pi-hole:
    container_name: pihole
    image: pihole/pihole:nightly
    restart: always
    hostname: pi.hole
    ports:
      - "80:80/tcp"
      - "127.0.0.1:53:53/tcp"
      - "127.0.0.1:53:53/udp"
      - "443:443/udp"
    volumes:
      - "/config/pihole:/etc/pihole"
      - "/config/dnsmasq:/etc/dnsmasq.d"
      - "/dev/null:/var/log/pihole.log:ro"
    environment:
      - DNS1=10.0.5.2#5054
      - IPv6=false
      - TZ=Europe/Moscow
      - DNSMASQ_LISTENING=all
      - WEBPASSWORD=adm1n
      - PIHOLELOG=/dev/null
      - VIRTUAL_HOST=pi.sanyuk.ru
    extra_hosts:
      - "host.mynetwork:$CIP"
      - "pihole.mynetwork:$CIP"
    networks:
      sanyuk_ru:
        ipv4_address: 10.0.5.3
    dns:
      - $CIP
      - 1.1.1.1
    cap_add:
      - NET_ADMIN
  openvpn:
     cap_add:
       - NET_ADMIN
     container_name: openvpn
     image: alekslitvinenk/openvpn
     restart: always
     ports:
       - "$CIP:1194:1194/udp"
       - "$CIP:1194:1194/tcp"
       - "8080:8080/tcp"
     environment:
        - HOST_ADDR="$CIP"
     restart: always
     networks:
       sanyuk_ru:
         ipv4_address: 10.0.5.4
networks:
  sanyuk_ru:
    driver: bridge
    ipam:
     config:
       - subnet: 10.0.5.0/29
EOF

docker-compose -f pi-hole-openvpn-cloudflare.yaml up -d
echo "::::::::::::::::::::::::::::::::::::::::::::::::::"

echo "Updating Flared PiVPN BlockLists."
echo ""
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "INSERT INTO adlist (address, enabled, comment) VALUES ('https://ewpratten.retrylife.ca/youtube_ad_blocklist/hosts.ipv4.txt', 1, 'YouTube IPv4');"
echo "YouTube IPv4 list added."
echo ""
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "INSERT INTO adlist (address, enabled, comment) VALUES ('https://github.com/kboghdady/youTube_ads_4_pi-hole/blob/master/youtubelist.txt', 1, 'YouTube');"
echo "YouTube list added."
echo ""
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "INSERT INTO adlist (address, enabled, comment) VALUES ('https://raw.githubusercontent.com/matomo-org/referrer-spam-blacklist/master/spammers.txt', 1, 'Spammers');"
echo "Spammers list added."
echo ""
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "INSERT INTO adlist (address, enabled, comment) VALUES ('https://v.firebog.net/hosts/AdguardDNS.txt', 1, 'AdGuard');"
echo "AdGuard list added."
echo ""
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "INSERT INTO adlist (address, enabled, comment) VALUES ('https://raw.githubusercontent.com/Ewpratten/youtube_ad_blocklist/master/blocklist.txt', 1, 'YouTube AD 1');"
echo "YouTube AD 1 list added."
echo ""
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "INSERT INTO adlist (address, enabled, comment) VALUES ('https://blocklistproject.github.io/Lists/ads.txt', 1, 'YouTube AD 2');"
echo "YouTube AD 2 list added."
echo ""
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "INSERT INTO adlist (address, enabled, comment) VALUES ('https://gist.githubusercontent.com/anudeepND/adac7982307fec6ee23605e281a57f1a/raw/5b8582b906a9497624c3f3187a49ebc23a9cf2fb/Test.txt', 1, 'YouTube AD 3');"
echo "YouTube AD 2 list added."
echo ""
echo "::::::::::::::::::::::::::::::::::::::::::::::::::"

docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^ad([sxv]?[0-9]*|system)[_.-]([^.[:space:]]+\.){1,}|[_.-]ad([sxv]?[0-9]*|system)[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^(.+[_.-])?adse?rv(er?|ice)?s?[0-9]*[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^(.+[_.-])?telemetry[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^adim(age|g)s?[0-9]*[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^adtrack(er|ing)?[0-9]*[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^advert(s|is(ing|ements?))?[0-9]*[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^aff(iliat(es?|ion))?[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^analytics?[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^banners?[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^beacons?[0-9]*[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^count(ers?)?[0-9]*[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^mads\.',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^pixels?[-.]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^stat(s|istics)?[0-9]*[_.-]',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?clicks\.beap\.bc\.yahoo\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?secure\.footprint\.net/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?match\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?clicks\.beap\.bc\.yahoo(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?sitescout(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?appnexus(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?evidon(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?mediamath(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?scorecardresearch(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?doubleclick(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?flashtalking(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?turn(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?mathtag(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?googlesyndication(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?s\.yimg\.com/cv/ae/us/audience/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?clicks\.beap/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?.doubleclick(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?yieldmanager(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?w55c(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?adnxs(\.\w{2}\.\w{2}|\.\w{2,4})/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?advertising\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?evidon\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?scorecardresearch\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?flashtalking\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?turn\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?mathtag\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?surveylink/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?info\.yahoo\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?ads\.yahoo\.com/',1);"
docker exec -it pihole sqlite3 /etc/pihole/gravity.db "Insert into domainlist (type, domain, enabled) values (3,'^https?://([A-Za-z0-9.-]*\.)?global\.ard\.yahoo\.com/',1);"
echo "RegEX added"
echo ""
echo "::::::::::::::::::::::::::::::::::::::::::::::::::"
echo "Adding Records."
docker exec -i openvpn sh -c "cat > /etc/openvpn/server.conf" << EOF
port 1194
proto udp
dev tun
ca /etc/openvpn/ca.crt
cert /etc/openvpn/MyReq.crt
key /etc/openvpn/MyReq.key
dh /etc/openvpn/dh.pem
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "redirect-gateway def1 bypass-dhcp"
push "block-outside-dns"
push "dhcp-option DNS 10.0.5.3"
#push "dhcp-option DNS 8.8.8.8"
duplicate-cn
keepalive 10 120
cipher AES-256-GCM
ncp-ciphers AES-256-GCM:AES-256-CBC
auth SHA512
user nobody
group nobody
persist-key
persist-tun
status openvpn-status.log
verb 1
tls-server
tls-version-min 1.2
tls-auth /etc/openvpn/ta.key 0


EOF

docker restart openvpn
docker exec -i openvpn apk add nano

echo "Added ALL Records."
echo "::::::::::::::::::::::::::::::::::::::::::::::::::"

echo "Updating Flared PiVPN Core."
docker exec pihole pihole -g
echo "Restarted Flared PiVPN Server."
echo "::::::::::::::::::::::::::::::::::::::::::::::::::"
#echo "Flared PiVPN Server terminated abnormally!"
#echo "::::::::::::::::::::::::::::::::::::::::::::::::::"
echo "Install Flared PiVPN finished."
#echo "::::::::::::::::::::::::::::::::::::::::::::::::::"
#echo "TO OPEN THE ADMIN WEBSITE - PRESS ANY BUTTON"
#echo "::::::::::::::::::::::::::::::::::::::::::::::::::"
RED='\033[0;31m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color
printf "I ${RED}love${NC} Sanyuk.ru\n"
printf "${YELLOW}Open http://$CIP/admin/ to manage Pi-Hole.${NC}\n"
printf "${YELLOW}One-time open http://$CIP:8080/ to get OpenVPN client configuration.${NC}\n"

