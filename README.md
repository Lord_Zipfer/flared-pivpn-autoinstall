# Flared PiVPN

**Say bye-bye to all ADs on any device! Windows, Linux, MacOS, Android and iOS are supported!**

Dockered Pi-hole + OpenVPN + Cloudflare + opts autoinstall script for Ubuntu 20.04+

## Getting started

To make it easy for you to get started with Flared PiVPN, here's a list of recommended next steps.
Prepare the Ubuntu OS:
```
sudo apt-update
sudo apt-get install -y docker.io docker-compose curl nano
```
Run the installation script:
```
sudo curl -s -L https://gitlab.com/Lord_Zipfer/flared-pivpn-autoinstall/-/raw/main/pi-hole.sh | sudo bash
```
After installation download and install the - [OpenVPN client](https://openvpn.net/vpn-client/) to your device and import the client configuration (*.ovpn file).
Enjoy!

## Adding new user
Run this command to genegate a new client configuration file.
```
docker exec openvpn ./genclient.sh
```
